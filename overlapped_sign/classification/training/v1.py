from __future__ import absolute_import, division, print_function, unicode_literals
# TensorFlow и tf.keras
import tensorflow as tf
from tensorflow import keras
# Вспомогательные библиотеки
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from PIL import Image
import util
from tensorflow.keras.models import save_model

IMAGES_PATH = "../../../" + "resources/frames_classification/"

def splitToImagesAndLabels(data):
	imagesArray = []
	labelsArray = np.array([], dtype=np.uint8)
	for index, row in data.iterrows():
		labelsArray = np.append(labelsArray, 1 if row["overlap"]>=util.overlapPercent() else 0) #0 - normal, 1 - overlapped
		im = Image.open(IMAGES_PATH + row["croped"])
		im2arr = np.array(im)
		im2arr = im2arr[:, :, 0]
		imagesArray.append(im2arr)
	return np.asarray(imagesArray), np.asarray(labelsArray)

train_data = pd.read_csv("train.csv")
test_data = pd.read_csv("test.csv")

train_images, train_labels = splitToImagesAndLabels(train_data)
test_images, test_labels = splitToImagesAndLabels(test_data)

#fashion_mnist = keras.datasets.fashion_mnist

#(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()


class_names = ['Normal', 'Overlapped']

print(train_images.shape)
print(test_images.shape)

print(len(train_labels))
print(len(test_labels))

print(class_names)
print(len(train_labels[train_labels==0]))
print(len(train_labels[train_labels==1]))
print(len(test_labels[test_labels==0]))
print(len(test_labels[test_labels==1]))
print(len(train_labels[train_labels==0])+len(test_labels[test_labels==0]))
print(len(train_labels[train_labels==1])+len(test_labels[test_labels==1]))


train_images = (train_images / 255.0)

test_images = (test_images / 255.0)

#plt.figure()
#plt.imshow(train_images[0])
#plt.colorbar()
#plt.grid(False)
#plt.show()

#plt.figure(figsize=(10,10))
#for i in range(25):
#    plt.subplot(5,5,i+1)
#    plt.xticks([])
#    plt.yticks([])
#    plt.grid(False)
#    plt.xlabel(class_names[train_labels[i]])
#    plt.imshow(train_images[i], cmap=plt.cm.binary)
#plt.show()

model = keras.Sequential([
    keras.layers.Flatten(input_shape=(len(train_images[0]), len(train_images[0]))),
    keras.layers.Dense(128, activation='relu'),
    keras.layers.Dense(len(class_names), activation='softmax')
])

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
			  
model.fit(train_images, train_labels, epochs=100)

test_loss, test_acc = model.evaluate(test_images,  test_labels, verbose=2)

save_model(model, '../../../models/sign_classification_model.h5')

#сколько из скольки мы угадываем правильно по классам
eval_images, eval_labels = util.prepareEvalData(train_images,train_labels,test_images,test_labels,0)
predictions = model.predict(eval_images)
util.calcTrueResultByClass(predictions, eval_labels, 0)

eval_images, eval_labels = util.prepareEvalData(train_images,train_labels,test_images,test_labels,1)
predictions = model.predict(eval_images)
util.calcTrueResultByClass(predictions, eval_labels, 1)


#для своего ознакомления на чем сработало, на чем нет
eval_images = [];
eval_images_orig = [];
eval_images_names = [];
eval_labels = [];
for index, row in train_data.iterrows():
	if row["overlap"]>=util.overlapPercent():
		eval_labels.append(1) #0 - normal, 1 - overlapped
		im = Image.open(IMAGES_PATH + row["croped"])
		im2arr = np.array(im)
		eval_images_orig.append(im2arr)
		im2arr = im2arr[:, :, 0]
		eval_images.append(im2arr)
		eval_images_names.append(row["croped"])
for index, row in test_data.iterrows():
	if row["overlap"]>=util.overlapPercent():
		eval_labels.append(1) #0 - normal, 1 - overlapped
		im = Image.open(IMAGES_PATH + row["croped"])
		im2arr = np.array(im)
		eval_images_orig.append(im2arr)
		im2arr = im2arr[:, :, 0]
		eval_images.append(im2arr)
		eval_images_names.append(row["croped"])
		
eval_images = np.asarray(eval_images)
eval_labels = np.asarray(eval_labels)
eval_images_orig = np.asarray(eval_images_orig)
predictions = model.predict(eval_images)

num_rows = 6
num_cols = 4
num_images = num_rows*num_cols
plt.figure(figsize=(2*2*num_cols, 2*num_rows))
for i in range(num_images):
  plt.subplot(num_rows, 2*num_cols, 2*i+1)
  util.plot_image(i, predictions, eval_labels, eval_images_orig, class_names,eval_images_names)
  plt.subplot(num_rows, 2*num_cols, 2*i+2)
  util.plot_value_array(i, predictions, eval_labels)
plt.show()