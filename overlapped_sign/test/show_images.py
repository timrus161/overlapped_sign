import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import random, os

FOLDER_WITH_IMAGES = "../../resources/rtsd-r1/test/"
FOLDER_WITH_IMAGES = "../recognize/temp/video2/"
FOLDER_WITH_IMAGES = "../../resources/rtsd-r1-overlapped/train/"
FOLDER_WITH_IMAGES = "../../temp2/"

fig = plt.figure()
columns = 10
rows = 10
for i in range(1, min(columns * rows + 1, len(os.listdir(FOLDER_WITH_IMAGES)))):
    img = random.choice([x for x in os.listdir(FOLDER_WITH_IMAGES)
                         if os.path.isfile(os.path.join(FOLDER_WITH_IMAGES, x))])
    fig.add_subplot(rows, columns, i)
    plt.imshow(mpimg.imread(FOLDER_WITH_IMAGES + img))  # rgb
# plt.imshow(mpimg.imread(FOLDER_WITH_IMAGES + img), cmap=plt.get_cmap('gray'),vmin=0,vmax=255) #black white


plt.show()
