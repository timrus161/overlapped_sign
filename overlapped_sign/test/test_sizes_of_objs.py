import pandas as pd

sample = "train"

full = pd.DataFrame()

for fileName in ["test.csv", "train.csv"]:
	path = fileName
	full = full.append(pd.read_csv(path))

print(len(full))
print(len(full.loc[full['overlap'] > 0]))

#full = full.groupby("filename")

small = 0
medium = 0
large = 0
print(32*32)
print(96*96)
dict = {}
for index, row in full.iterrows():
	area = int(row['height']) * int(row['width'])
	#print(area)
	key = '';
	if area < 32*32:
		small = small + 1
		key = 'small'
	elif area < 96*96:
		medium = medium + 1
		key = 'medium'
	else:
		large = large + 1
		key = 'large'
	if row['filename'] not in dict:
		dict[row['filename']] = set()
	dict[row['filename']].add(key)
for key, value in dict.items():
	if len(value) == 3:
		print(key)

print(small)
print(medium)
print(large)
