import pandas as pd
from os import listdir
from os.path import isfile, join
import cv2

ROOT = "../../resources/"
ORIG_ROOT = ROOT+"frames/train"
allImages = [f for f in listdir(ORIG_ROOT) if isfile(join(ORIG_ROOT, f))]

for img in allImages:
	print(img)
	image_file = cv2.imread(ORIG_ROOT+"/"+img)
	#image_file = cv2.cvtColor(image_file, cv2.COLOR_BGR2GRAY)
	image_file = image_file[:, :, 0]
	cv2.imwrite(ROOT+"frames_blackwhite/train/"+img, image_file) 
