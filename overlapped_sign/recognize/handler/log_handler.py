from handler.handler import Handler


class LogHandler(Handler):
    def __init__(self, output):
        self.out = open(output, "w+")

    def handle(self, frame, boxes, scores, classes, num):
        max_boxes_to_draw = boxes.shape[0]
        items = []
        for i in range(min(max_boxes_to_draw, boxes.shape[0])):
            if scores[i] > 0.60:
                items.append((boxes[i], classes[i], scores[i]))
        buses = []
        persons = []
        for item in items:
            if item[1] == 2:
                persons.append(item)
            else:
                buses.append(item)

        for person in persons:
            box = person[0]
            for bus in buses:
                bbox = bus[0]
                if (bbox[2] - 0.15 < box[0] < bbox[2] + 0.15) and (bbox[3] + 0.03 > box[3] > bbox[3] - 0.03):
                    print(items)
                    timestamp = str(datetime.timedelta(milliseconds=frame.time_code))
                    print(timestamp)
                    self.out.write(timestamp + "\n")

        super().handle(frame, boxes, scores, classes, num, )

    def close(self):
        self.out.close()
        super().close()