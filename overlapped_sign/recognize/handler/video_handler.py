from recognize.handler.handler import Handler
import cv2
import numpy as np
import PIL.Image as Image
import time
import tensorflow as tf

IMG_SIZE = 48


class VideoRecordHandler(Handler):

    def __init__(self, output, category_index, source_path, classification_model):
        video = cv2.VideoCapture(source_path)
        width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.fps = int(video.get(cv2.CAP_PROP_FPS))
        video.release()
        codec = cv2.VideoWriter_fourcc(*"mp4v")
        fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        # self.out = cv2.VideoWriter(output, codec, fps, (width, height))
        self.out = cv2.VideoWriter('output.avi', cv2.VideoWriter_fourcc('m','p','4','v'), self.fps, (width, height))
        self.category_index = category_index
        self.model = classification_model

    def handle(self, frame, boxes, scores, classes, num, cur_frame):
        img = frame.image.copy()
        boxes = np.squeeze(boxes)
        scores = np.squeeze(scores)
        classes = np.squeeze(classes).astype(np.int32)

        for i in range(len(boxes)):
            if scores[i] < 0.91:
                continue

            ymin, xmin, ymax, xmax = boxes[i]

            im_height, im_width, shp = frame.image.shape
            (x1, x2, y1, y2) = (int(xmin * im_width), int(xmax * im_width),
                                int(ymin * im_height), int(ymax * im_height))
            sign_width = x2 - x1
            sign_height = y2 - y1
            # if sign_width < 34 or sign_height < 34\
            #         or sign_width > 52 or sign_height > 52:
            #     print("skip", sign_width, sign_height, x1, y1, x2, y2)
            #     continue
            # if abs((x2 - x1) - (y2 - y1)) > 5:
            #     print("not square skip", x2 - x1, y2 - y1, x1, y1, x2, y2)
            #     continue

            # crop_img = frame.image[y1:y2, x1:x2]
            crop_img = frame.image[max(y1 - 3, 0):min(y2 + 3, im_height), max(x1 - 3, 0):min(x2 + 3, im_width)]
            saved_orig = crop_img[:]
            crop_img = cv2.resize(crop_img, (IMG_SIZE, IMG_SIZE))

            img_tensor = tf.convert_to_tensor(crop_img, dtype=tf.float32)
            img_tensor = (img_tensor / 127.5) - 1

            imageArray = np.asarray([img_tensor])

            prediction = self.model.predict(imageArray)
            print((prediction > 0.5).astype("int32"))
            prediction = prediction[0]
            print((prediction > 0.5).astype("int32"))

            prediction_class = self.model.predict_classes(imageArray)[0]

            # if np.argmax(prediction) == 0:
            #	continue
            if prediction_class != 1:  # если не закрыт, то пропускаем
                print("skip", prediction, x2 - x1, y2 - y1, x1, y1, x2, y2)
                continue
            # if prediction[0] < 5:
            #     print("skip", prediction, x2 - x1, y2 - y1, x1, y1, x2, y2)
            #     continue
            print("ok", prediction)
            timr = str(time.time())
            cv2.rectangle(img, (x1, y1), (x2, y2), (0,255,0) if prediction_class == 1 else (255,0,0), 2)
            cv2.imwrite("temp/" + str(round(cur_frame/self.fps,2)) + "_" + str(prediction_class) + "_" + str(prediction) + " " + timr + " full.jpg", img)
            # cv2.imwrite("temp/" + str(cur_frame/self.fps) + "_" + str(prediction) + " " + timr + " full.jpg", frame.image.copy())
            cv2.imwrite("temp/" + str(round(cur_frame/self.fps,2)) + "_" + str(prediction_class) + "_" + str(prediction) + " " + timr + " mini.jpg", saved_orig)
        # cv2.putText(img, self.category_index[classes[i]]["name"], (x1 - 10, y1 - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)

        # All the results have been drawn on the frame, so it's time to display it.
        self.out.write(img)
        super().handle(frame, boxes, scores, classes, num, cur_frame)

    def close(self):
        super().close()
