from recognize.handler.handler import Handler
import numpy as np
import cv2
import PIL.Image as Image
import time
import tensorflow as tf

IMG_SIZE = 48


class DisplayHandler(Handler):

    def __init__(self, category_index, classification_model):
        self.category_index = category_index
        self.model = classification_model

    def handle(self, frame, boxes, scores, classes, num):
        img = frame.image.copy()
        boxes = np.squeeze(boxes)
        scores = np.squeeze(scores)
        classes = np.squeeze(classes).astype(np.int32)
        for i in range(len(boxes)):
            if scores[i] < 0.91:
                continue

            ymin, xmin, ymax, xmax = boxes[i]
            im_width, im_height = Image.fromarray(np.uint8(frame.image)).convert('RGB').size
            (x1, x2, y1, y2) = (int(xmin * im_width), int(xmax * im_width),
                                int(ymin * im_height), int(ymax * im_height))
            if x2 - x1 < 30 or y2 - y1 < 30:
                print("skip", x2 - x1, y2 - y1, x1, y1, x2, y2)
                continue
            if abs((x2 - x1) - (y2 - y1)) > 5:
                print("not square skip", x2 - x1, y2 - y1, x1, y1, x2, y2)
                continue

            crop_img = frame.image[y1:y2, x1:x2]
            # saved_orig = crop_img[:]
            crop_img = cv2.resize(crop_img, (IMG_SIZE, IMG_SIZE))

            img_tensor = tf.convert_to_tensor(crop_img, dtype=tf.float32)
            img_tensor = (img_tensor / 127.5) - 1

            imageArray = np.asarray([img_tensor])

            prediction = self.model.predict(imageArray)
            print((prediction > 0.5).astype("int32"))
            prediction = prediction[0]
            print((prediction > 0.5).astype("int32"))

            prediction_class = self.model.predict_classes(imageArray)[0]

            # if np.argmax(prediction) == 0:
            #	continue
            if prediction_class != 1:  # если не закрыт, то пропускаем
                print("skip", prediction, x2 - x1, y2 - y1, x1, y1, x2, y2)
                continue
            if prediction[0] < 5:
                print("skip", prediction, x2 - x1, y2 - y1, x1, y1, x2, y2)
                continue
            # print("ok", prediction)
            # timr = str(time.time())
            # cv2.imwrite("temp/" + str(prediction) + " " + timr + " full.jpg", frame.image.copy())
            # cv2.imwrite("temp/" + str(prediction) + " " + timr + " mini.jpg", saved_orig)
            cv2.rectangle(img, (x1, y1), (x2, y2), (36, 255, 12), 2)
        # cv2.putText(img, self.category_index[classes[i]]["name"], (x1 - 10, y1 - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)

        cv2.imshow('Object detector', img)
        super().handle(frame, boxes, scores, classes, num)
