import cv2
import time
import os


timeIn = time.time()

fileName = "video/20160101000418027.MP4"
fileName = "video/20160101000536028.MP4"

vidcap = cv2.VideoCapture(fileName)
fps, wh = vidcap.get(cv2.CAP_PROP_FPS), (int(vidcap.get(cv2.CAP_PROP_FRAME_WIDTH)),int(vidcap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
print("in", fps, wh)
fourcc = cv2.VideoWriter_fourcc(*'mp4v')

outputFileName = fileName+'_output.MP4'
if os.path.exists(outputFileName):
	os.remove(outputFileName)
vidOut = cv2.VideoWriter(outputFileName, fourcc, fps, wh)


success, image = vidcap.read()
count = 0
while success:

	x, y, w, h = 100, 100, 100, 100
	cv2.rectangle(image, (x, y), (x + w, y + h), (36,255,12), 2)
	cv2.putText(image, "class-name", (x - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)

	vidOut.write(image)

	success, image = vidcap.read()
	#print('Read a new frame: ', success)
	count += 1

vidcap.release()
vidOut.release()
print("time ::", time.time() - timeIn)
